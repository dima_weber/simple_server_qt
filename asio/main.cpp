#include <iostream>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>

// http://www.gamedev.net/blog/950/entry-2249317-a-guide-to-getting-started-with-boostasio/?pg=5

boost::mutex global_stream_lock;

void workerThread(boost::shared_ptr<boost::asio::io_service> io_service)
{
    global_stream_lock.lock();
    std::cout << "[" << boost::this_thread::get_id() << "] thread started." << std::endl;
    global_stream_lock.unlock();

    io_service->run();

    global_stream_lock.lock();
    std::cout << "[" << boost::this_thread::get_id() << "] thread finished." << std::endl;
    global_stream_lock.unlock();
}

void dispatch(int x)
{
    global_stream_lock.lock();
    std::cout << "[" << boost::this_thread::get_id() << "] " << __func__ << " x = " << x << std::endl;
    global_stream_lock.unlock();
}

void post(int x)
{
    global_stream_lock.lock();
    std::cout << "[" << boost::this_thread::get_id() << "] " << __func__ << " x = " << x << std::endl;
    global_stream_lock.unlock();
}

void run(boost::shared_ptr<boost::asio::io_service> io_service)
{
    for (int i=0; i<3; i++)
    {
        io_service->dispatch(boost::bind( &dispatch, i*2));
        io_service->post(boost::bind(&post, i*2+1));
        boost::this_thread::sleep( boost::posix_time::milliseconds (1000));
    }
}

int main(int argc, char *argv[])
{
    boost::shared_ptr<boost::asio::io_service> io_service ( new boost::asio::io_service);
    boost::shared_ptr<boost::asio::io_service::work> work ( new boost::asio::io_service::work(*io_service));

    global_stream_lock.lock();
    std::cout << "[" << boost::this_thread::get_id() << "] the program will exit when all work has finished." << std::endl;
    global_stream_lock.unlock();

    boost::thread_group worker_threads;
    for(int i=0; i<1; i++)
    {
        worker_threads.create_thread(boost::bind( &workerThread, io_service));
    }

    io_service->post ( boost::bind (&run, io_service));

    work.reset();

    worker_threads.join_all();

    return 0;
}
