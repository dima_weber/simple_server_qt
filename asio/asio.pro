#-------------------------------------------------
#
# Project created by QtCreator 2014-06-26T14:43:37
#
#-------------------------------------------------

QT       -= gui core

TARGET = asio
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += /usr/local/prereq/boost/include

LIBS += -L /usr/local/prereq/boost/lib \
        -lboost_system \
        -lboost_thread

QMAKE_CXXFLAGS += -Wno-unused-local-typedefs
SOURCES += main.cpp
