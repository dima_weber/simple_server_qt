// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


#include <winsock2.h>
#include <ws2tcpip.h>

#include <string>
#include <iostream>
#include <list>

#pragma comment(lib, "Ws2_32.lib")

using namespace std;

void writeLog (const string& msg, const string& file = "", int line = 0, const string& func = "")
{
/*
	if (!file.empty())
        cout << file;
	if (line > 0)
        cout << "[" << line << "]";
*/
	if (!func.empty())
		cout << func;
	cout << "\t";
	cout << msg << endl;
}
#define LOG(s) writeLog (s, __FILE__, __LINE__, __FUNCTION__);


class Server 
{
	WSADATA wsaData;
	SOCKET mSocket;
	string sPort;
	addrinfo* pAddress;
	bool quitAsked;
	list<SOCKET> clients;
	typedef list<SOCKET>::iterator ClientIterator;

public:
	Server(int port);
	~Server();

	bool listen();
	void processConnections();
protected:
	void sendMessage (const string& msg, SOCKET client);
	void broadcastMessage (const string& msg);
	void processMessage (const string& msg);
	void createNewClientConnection();
	string readClientMessage(SOCKET sock);
	void removeClosedSockets();
	static bool socketIsInvalid (SOCKET sock);
};

Server::Server(int port)
{
	LOG ("starating server");

	mSocket = INVALID_SOCKET;
	sPort = to_string(port);
	pAddress = NULL;
	if (WSAStartup(MAKEWORD(2,2), &wsaData))
	{
		throw string("fail to initialize WSA");
	}
}

Server::~Server()
{
	ClientIterator it;
	for(it = clients.begin(); it != clients.end(); it++)
		closesocket(*it);

	if (mSocket != INVALID_SOCKET)
		closesocket(mSocket);
	if (pAddress != NULL)
		freeaddrinfo(pAddress);
	WSACleanup();

	LOG("server stopped");
}

bool Server::listen()
{
	addrinfo hints;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	int ret = getaddrinfo(NULL, sPort.c_str(), &hints, &pAddress);
	if (ret)
	{
		LOG ("getaddrinfo fail");
		return false;
	}

	mSocket = ::socket (pAddress->ai_family, pAddress->ai_socktype, pAddress->ai_protocol);
	if (mSocket == INVALID_SOCKET)
	{
		LOG ("Error creating socket" + WSAGetLastError());
		return false;
	}

//	int aliveToggle = 1;
//	setsockopt (mSocket, SOL_SOCKET, SO_KEEPALIVE, (char*)&aliveToggle, sizeof(aliveToggle));

	ret = ::bind (mSocket, pAddress->ai_addr, (int)pAddress->ai_addrlen);
	if (ret == SOCKET_ERROR)
	{
		LOG ("bind failed : " + WSAGetLastError());
		return false;
	}

	if ( ::listen( mSocket, SOMAXCONN ) == SOCKET_ERROR ) 
	{
		LOG( "Listen failed with error: " + WSAGetLastError() );
		return false;
	}

	LOG ("server is listening now");

	return true;
}

void Server::processConnections()
{
	fd_set readset;
	int ret;
	ClientIterator it;
	quitAsked = false;

	while (!quitAsked)
	{
		do 
		{
			FD_ZERO (&readset);
			FD_SET(mSocket, &readset);
			for (it = clients.begin(); it != clients.end(); it++)
			{
				if (*it != INVALID_SOCKET)
					FD_SET(*it, &readset);
			}

			ret = select (0, &readset, 0,0, NULL);
		} while (ret == -1);

		LOG ("some activity detected");
		if (ret > 0)
		{
			if ( FD_ISSET(mSocket, &readset))
			{
				createNewClientConnection();
			}
			for (it = clients.begin(); it != clients.end(); it ++)
			{
				if (FD_ISSET(*it, &readset))
				{
					string msg = readClientMessage(*it);
					if (msg.empty())
						*it = INVALID_SOCKET;
					else
						processMessage(msg);
				}
			}
		}

		removeClosedSockets();
	}
}

void Server::processMessage(const string& msg)
{
	LOG("processing message " + msg);
	if (msg == "hi\r\n")
	{
		broadcastMessage("hello");
	}
	else if (msg == "shutdown\r\n")
	{
		broadcastMessage("server is going down");
		quitAsked = true;
	}
	else if (msg == "time\r\n")
	{
		TCHAR buff[100];
		int buffLen = sizeof(buff);
		memset(buff, 0, buffLen);
		SYSTEMTIME sysTime;
		GetSystemTime(&sysTime);
		GetTimeFormat(LOCALE_SYSTEM_DEFAULT, 0, &sysTime, NULL, buff, buffLen);
		broadcastMessage((char*)buff);
	}
}

void Server::sendMessage(const string& msg, SOCKET client)
{
	static char LRCR [] = "\r\n";
	LOG("sending message " + msg + "to client " + to_string((int)client));
	int ret = ::send (client, msg.c_str(), msg.length(), 0);
	if (ret == -1)
	{
		cout << "client disconnected";
		closesocket(client);
	}
	ret = ::send (client, LRCR, sizeof(LRCR), 0);
}

void Server::broadcastMessage(const  string& msg)
{
	LOG("broadcat message " + msg);
	ClientIterator it;
	for (it = clients.begin(); it != clients.end(); it ++)
	{
		if (*it != INVALID_SOCKET)
			sendMessage(msg, *it);
	}
}

void Server::createNewClientConnection()
{
	LOG("new connection");

	sockaddr_in clientSocketAdderss;
	SOCKET clientSocket ;
	int clientSocektAddress_len = sizeof(clientSocketAdderss);
	clientSocket = ::accept(mSocket, (sockaddr*)&clientSocketAdderss, &clientSocektAddress_len);
	int aliveToggle = 1;
	setsockopt (clientSocket, SOL_SOCKET, SO_KEEPALIVE, (char*)&aliveToggle, sizeof(aliveToggle));
	
	clients.push_back (clientSocket);
}

string Server::readClientMessage(SOCKET sock)
{
	string msg;
	char msgBuff[100];
	int msgBuffLen = 100;
	LOG("client " + to_string(int(sock)) + " sent message");
	memset(msgBuff, 0, msgBuffLen);
	int ret = ::recv(sock, msgBuff, msgBuffLen, 0);
	if (ret > 0)
	{
		msg = msgBuff;
	}
	else if (ret == 0)
	{
		LOG("client disconnected");
		closesocket(sock);
	}
	else
		LOG("socket error");

	return msg;
}

void Server::removeClosedSockets()
{
	clients.remove_if (Server::socketIsInvalid);
}

bool Server::socketIsInvalid(SOCKET sock)
{
	return sock == INVALID_SOCKET;
}
	
int main(int argc, char* argv[]) 
{
	int port;
	try 
	{
		if (argc < 2)
			throw  string("not enought parameters");

		port = strtol (argv[1], NULL, 10);
		if (port == 0)
			throw  string("wrong port number");

		Server server (port);

		if (!server.listen())
			throw  string("server listen fail");

		server.processConnections();
	}
	catch (const string& errMsg)
	{
		LOG(errMsg);
		return 1;
	}

	return 0;
}
