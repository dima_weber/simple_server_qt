#include "server.h"
#include <QTcpSocket>
#include <QDateTime>

Server::Server(int port, QObject *parent) :
    QTcpServer(parent), port(port)
{
    qDebug() << "server created";
    connect (this, SIGNAL(newConnection()), SLOT(onNewConnection()));
}

void Server::onNewConnection()
{
    QTcpSocket* pConnection = nextPendingConnection();
    connections.push_back( pConnection );

    qDebug() << "new connection established on port" << pConnection->peerPort() << "from address" << pConnection->peerAddress();
    sendMessage("connection established", pConnection);

    connect (pConnection, SIGNAL(readyRead()), SLOT(onMessage()));
}

void Server::onMessage()
{
    QTcpSocket* pClient = dynamic_cast<QTcpSocket*>(sender());

    QString cmd = QString::fromUtf8(pClient->readLine()).simplified();
    qDebug() << "new message" << cmd << "recieved";
    if (cmd == "hi")
        broadcastMessage("hello");
    else if (cmd == "time")
        broadcastMessage(QDateTime::currentDateTime().toUTC().toString());
    else if (cmd == "shutdown")
        shutdown();
    else
        sendMessage("unknown command", pClient);
}

void Server::shutdown()
{
    broadcastMessage("server is goind down");
    std::list<QTcpSocket*>::iterator it;
    for (it = connections.begin(); it != connections.end(); it++)
    {
        (*it)->close();
        qDebug() << "connection to client" << *it << "closed";
    }
    close();
    qDebug() << "server finished";
    emit finished();
}

bool Server::start()
{
    bool ok;
    ok = listen(QHostAddress::Any, port);
    if (ok)
        qDebug () << "server started";
    return ok;
}

void Server::sendMessage(const QString &msg, QTcpSocket* pClient)
{
    qDebug() << "sending message" << msg << "to client" << pClient;
    if (pClient->state() == QTcpSocket::ConnectedState)
    {
        pClient->write(msg.toUtf8().constData());
        pClient->write("\r\n");
    }
    else
    {
        "fail: client not connected";
    }
}

void Server::broadcastMessage(const QString &msg)
{
    qDebug() << "broadcast message" << msg;
    std::list<QTcpSocket*>::iterator it;
    for (it = connections.begin(); it != connections.end(); it++)
    {
        sendMessage(msg, *it);
    }
}
