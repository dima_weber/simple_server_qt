#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>
#include <list>

class QTcpSocket;

class Server : public QTcpServer
{
    Q_OBJECT
    std::list<QTcpSocket*> connections;
    int port;
public:
    explicit Server(int port, QObject *parent = 0);

signals:
    void finished();

public slots:
    void onNewConnection();
    void onMessage();
    void shutdown();
    bool start();
    void sendMessage(const QString& msg, QTcpSocket* pClient);
    void broadcastMessage (const QString& msg);
};

#endif // SERVER_H
