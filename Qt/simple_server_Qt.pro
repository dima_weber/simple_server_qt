#-------------------------------------------------
#
# Project created by QtCreator 2014-06-20T21:47:21
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = simple_server_Qt
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    server.cpp

HEADERS += \
    server.h
