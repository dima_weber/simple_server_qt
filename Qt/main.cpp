#include <QCoreApplication>
#include "server.h"
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int port;
    if (argc < 2)
    {
        qDebug() << "not enought parameters to start";
        return 1;
    }
    bool ok = false;
    port = QString::fromUtf8(argv[1]).toInt(&ok);

    if (!ok)
    {
        qDebug() << "wrong port number";
        return 2;
    }


    Server server(port);

    a.connect (&server, SIGNAL(finished()), SLOT(quit()));

    if (!server.start())
    {
        qDebug() << "fail to start server";
        return 3;
    }

    return a.exec();
}
